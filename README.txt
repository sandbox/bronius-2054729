Ping is designed to respond to up checks or "pings". The module ensures that 
Drupal is able basically functioning as it retreieves data from the database. 
The response message is configurable via an admin form.  The appropriate headers
are set to ensure the response isn't cached.

Ping will not be backported to Drupal 6 as the namespace is taken by a core 
module with very different functionality.  This module is not the Drupal 7
successor or a port of the ping module from Drupal 6 core.
